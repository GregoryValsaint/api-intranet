<?php

namespace App\Entity;

use App\Repository\OutilRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=OutilRepository::class)
 */
class Outil
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $intitulé;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionOutil;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="outil")
     */
    private $user;

    public function __construct()
    {
        $this->user = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitulé(): ?string
    {
        return $this->intitulé;
    }

    public function setIntitulé(?string $intitulé): self
    {
        $this->intitulé = $intitulé;

        return $this;
    }

    public function getDescriptionOutil(): ?string
    {
        return $this->descriptionOutil;
    }

    public function setDescriptionOutil(?string $descriptionOutil): self
    {
        $this->descriptionOutil = $descriptionOutil;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->setOutil($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->user->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getOutil() === $this) {
                $user->setOutil(null);
            }
        }

        return $this;
    }
}
